<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_script("PDF_script","https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js","","");
    wp_enqueue_script("site_script",get_stylesheet_directory_uri()."/script.min.js","","",1);
    wp_enqueue_script("m2FlooringCalculator","https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js","","");
    wp_enqueue_script("shareBox-script", get_stylesheet_directory_uri()."/resources/sharebox/needsharebutton.js","","");
    wp_enqueue_script("search",get_stylesheet_directory_uri()."/search.js","","",1);
});

// function replace_core_jquery_version() {
//     wp_deregister_script( 'jquery' );
//     // Change the URL if you want to load a local copy of jQuery from your own server.
//     wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js", array(), '3.2.1' );
// }
// add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
              /* font-size:2.5em !important; */
              font-size: 36px !important;
           }
      </style>  
   <?php 
}


$data ='';

$ddsa = unserialize(stripslashes($data));


add_action( 'wp_ajax_nopriv_add_fav_product', 'add_fav_product' );
add_action( 'wp_ajax_add_fav_product', 'add_fav_product' );

function add_fav_product() {
    $is_fav = $_POST['is_fav'];
    $post_id = $_POST['post_id'];

    $result = update_post_meta( $post_id, 'is_fav', $is_fav);
if($result == false){
    $result1 = add_post_meta( $post_id, 'is_fav', $is_fav);
}
}

add_action( 'wp_ajax_nopriv_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );
add_action( 'wp_ajax_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );

function base64_to_jpeg_convert() {    
    global $wpdb;

    $upload_dir = wp_get_upload_dir();
    $output_file= $upload_dir['basedir']. '/measure/'.uniqid().'.png';

    $img = $_POST['imagedata']; 
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    file_put_contents($output_file, $data);
    
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
      //  write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'userid' => $current_user->ID, 
            'image_name' => $_POST['imagename'],
            'image_path' => $output_file 
        );    
    
         $wpdb->insert( 'wp_measure_images', $data);

         $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

       // write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;


        wp_die();
    }

}

function measurement_tool_images($arg){    
    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
       // write_log( 'Personal Message For '. $current_user->ID ) ;

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

       // write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';
        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";
        

        return $content;
    }
   
  
}    
add_shortcode('measurementtool_images', 'measurement_tool_images');


add_action( 'wp_ajax_nopriv_delete_measureimg', 'delete_measurement_images' );
add_action( 'wp_ajax_delete_measureimg', 'delete_measurement_images' );

function delete_measurement_images() {  
    global $wpdb;

    $mid = $_POST['mimg_id'];

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
       // write_log( 'Personal Message For '. $current_user->ID ) ;

        $wpdb->get_results('DELETE FROM wp_measure_images WHERE userid = '.$current_user->ID.' and id = '.$mid.'');
        
      // write_log('DELETE FROM wp_measure_images WHERE userid = '.$current_user->ID.' and id = '.$mid.'');
        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

       // write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="'.home_url().''.$img_path.'" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="javascript:void(0)" title="#" onclick="openPopUp(this)"   data-img="'.home_url().''.$img_path.'" data-title="'.$img->image_name.'" onclik="openPopUp(e)">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;
    }

  wp_die();

}

add_action( 'wp_ajax_nopriv_add_favroiute', 'add_favroiute' );
add_action( 'wp_ajax_add_favroiute', 'add_favroiute' );

function add_favroiute() {

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
       // write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
         $wpdb->insert( 'wp_favorite_posts', $data);        

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }
}

add_action( 'wp_ajax_nopriv_remove_favroiute', 'remove_favroiute' );
add_action( 'wp_ajax_remove_favroiute', 'remove_favroiute' );

function remove_favroiute() {    

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
       // write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
        $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE user_id = '.$_POST['user_id'].' and product_id = '.$_POST['post_id'].'');

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }

}


function greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

   // write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

   // write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog', 'instock_laminate');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Instock Laminate"=>"instock_laminate",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="javascript:void(0)" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
         //   write_log('check_note');
         //   write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossOrigin="Anonymous" class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        // $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }
        echo '</div></div>';
        wp_reset_postdata();
    return $content;

 }


}
add_shortcode('greatcustom_favorite_posts', 'greatcustom_favorite_posts_function');


add_action( 'wp_ajax_nopriv_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
add_action( 'wp_ajax_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
function remove_greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE product_id = '.$_POST['post_id'].' and user_id = '.get_current_user_id().'');    

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

   // write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

   // write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            //write_log('check_note');
          //  write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossOrigin="Anonymous" class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;wp_reset_postdata();
            $content .= "</div></div>";
        
    
        }

        $content .='</div></div>';
        

    echo $content;

    wp_die();

 }
}


add_action('wp_ajax_add_note_form', 'add_note_form');
add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');

function add_note_form()
{
    global $wpdb;

    $fav_sql = 'UPDATE wp_favorite_posts SET note = "'.$_POST['note'].'" WHERE user_id = '.get_current_user_id().' and product_id = '.$_POST['addnote_productid'].'';
            
    $check_fav = $wpdb->get_results($fav_sql); 

    echo $fav_sql;
}
//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/tile-products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }

    return $links;
}